package main

import (
	"fmt"
	"log"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path[1:] {
	case "new":
		link := "https://deutsche-it-schule.com.ua"
		fmt.Fprintf(w, "<a href=%s target=popup onclick=window.open('%s','popup','width=600,height=600'); return false;>Cделай что-то новенькое... Жми!</a>", link, link)
	default:
		fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
	}
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
